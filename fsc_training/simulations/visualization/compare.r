
#Function to normalize after removal of monomorphic sites
norm <- function(x) x/sum(x);


#################Processing the SFS from analysis01 (equilibrium model)  ###############################################

#Read in the SFS from the equilibrium model (no pop. 2);
#Removing the monomorphic classes by subsetting from 2 to 40
sfs_anal01<-scan("analysis01_DAFpop0.obs", skip=2);
sfs_anal01_mono<-sfs_anal01[2:(length(sfs_anal01)-1)];
#normalizing to be able to compare to with SFSs composed of different total number of polymorphic sites
sfs_anal01_mono_norm <- norm(sfs_anal01_mono);


#################Processing the SFS from analysis02 (expansion model)  ###############################################

#Read in the SFS from the expansion model (pop. size increase)
sfs_anal02<-scan("analysis02_DAFpop0.obs", skip=2);
#Removing the monomorphic classes by subsetting from 2 to 40
sfs_anal02_mono<-sfs_anal02[2:(length(sfs_anal02)-1)];
#normalizing to be able to compare to SFS composed of a different total number of polymorphic sites
sfs_anal02_mono_norm <- norm(sfs_anal02_mono);


#################Processing the SFS from analysis03 (crash model)  ###############################################

#Read in the SFS from the crash model (pop. size decrease)
sfs_anal03<-scan("analysis03_DAFpop0.obs", skip=2);
#Removing the monomorphic classes by subsetting from 2 to 40
sfs_anal03_mono<-sfs_anal03[2:(length(sfs_anal03)-1)];
#normalizing to be able to compare to SFS composed of a different total number of polymorphic sites
sfs_anal03_mono_norm <- norm(sfs_anal03_mono);


#Creating and printing the barplot in a pdf format
pdf("equilibrium_vs_expansion.pdf", paper="a4r");
barplot(rbind(sfs_anal02_mono_norm, sfs_anal01_mono_norm, sfs_anal03_mono_norm), beside=T, col=rainbow(3));
legend("topright", legend=c("expansion", "equilibrium", "crash"), fill=rainbow(3));
dev.off();



