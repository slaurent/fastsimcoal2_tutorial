//Number of population samples (demes)
1
//Population effective sizes (number of genes)
N_PRESENT
//Sample sizes
40
//Growth rates
0
//Number of migration matrices : 0 implies no migration between demes
0
//Historical events: time, source, sink, migrants, new size, growth rate, migr. matrix
1 historical event
TIME 0 0 0 REL_SIZE 0 0
//Number of independent loci (contigs), same (0) or differents (1) contig structures
1 0
//Per chromosome: Number of linkage blocks
1
//per Block: data type, num loci, rec. rate and mut rate + optional parameters
FREQ 1 0 1e-8 OUTEXP
