---
title: "An introduction to the inference of demographic models using fastsimcoal2"
author: "stefan laurent"
date: "4/24/2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

##Exercise 1

The aim of this exercise is to teach you how to use the *.par* files to specify the parameters of a
demographic model and the structure of the simulated genetic dataset. You will then simulate the
same dataset using three different demographic models: an equilibrium, an expansion, and a
population size crash model. For each of these demographic scenarios we will calculate the site
frequency spectrum, (SFS, i.e the distribution of allele frequencies in the simulated dataset) and
visualize them using R. The differences between the SFS will be discussed in the classroom.

```{}
#in terminal
#Copy the folder fsc_training located in /home/stefan (evopserver) to your working environment.
#enter the folder
>cd fsc_training
```


The folder *simulations* contains all the input files and scripts that you will need to do this exercise.
The file *simulations_index.csv* lists and explains the content of the folders located in the folder
*simulations*.

```{}

>cat simulations_index.csv
```

Go to the folder that contains the input files for the equilibrium model (analysis01). In this model
the population size doesn't change.

```{}
>cd simulations/analysis01
```

For the simulations we need the binary itself (fsc26) and the input file (analysis01.par). Let's
start by printing analysis01.par to the screen

```{}
>cat analysis01.par
```

This is what should appear on your screen:

```{}
//Number of population samples (demes)
1
//Population effective sizes (number of genes)
100000 #myComment: number of copies of a gene. For diploid organisms this corresponds to 50000 individuals (assuming autosomal data)
//Sample sizes
40
//Growth rates
0
//Number of migration matrices : 0 implies no migration between demes
0
//Historical events
0 historical event
//Number of independent loci (contigs), same (0) or differents (1) contig structures
20000 0 #myComment: 20000 contigs , all identical
//Per chromosome: Number of linkage blocks
1 #Every contig is evolving independently from the others. There is free recombination between contigs
//per Block: data type, num loci, rec. rate and mut rate + optional parameters
DNA 300 1e-8 1e-8
```

Note: Refer to the fsc2 manual for more information about template files or ask the teacher
Now we need to launch fsc2 with the input file *analysis01.par*. We can launch it by directly entering
the following command on the terminal.

```{}
>fsc26 -i analysis01.par -n 1 -s 0 --dsfs -I
```

Or we can write this command in a file and execute it as a shell script (e.g. launch_fsc2.sh) and
execute it as follow:

```{}
>bash launch_fsc2.sh
```

By writing commands into shell scripts, even for a single line, you will always be sure to be able to
reproduce the simulated results by re-executing the initial command. Check the meaning of the “-
n”, “-s”, “--dsfs”, and “-I” flags in the manual (p.44).

Now let´s have a look at the files that fsc2 created. Enter the newly created folder and print the file
containing the unfolded SFS (analysis01_DAFpop0.obs)

```{}
>cd analysis01
>cat analysis01_DAFpop0.obs
```

This is the SFS. The first column (d0_0) are all nucleotidic positions in the simulated alignment that
carry a single allele. These sites are called “monomorphic”. The second column (d0_1) are all
positions that carry 2 alleles and where the new allele (which is called the derived allele) is
observed in only one DNA sequence. These sites are called “singletons”. The second
column counts all sites where 2 alleles are observed and where the derived allele is observed in
exactly 2 DNA sequences. These sites are called “doubletons”. And so on …

Now go out of the *analysis01* folder and repeat he same analysis in the folder *analysis02*
(population expansion) and *analysis03* (Population size crash). Take your time to analyze the
*analysis02.par* and *analysis03.par* files. They illustrate how population size can be set to vary at a specific
point in time. Refer to the manual for more information about the modeling of population size
changes or ask the teacher.

Now that we have generated the SFS under three different models we will create a graph to
be able to visualize the differences between them. To do this start by copying the SFSs
(analysisX_DAFpop0.obs) to the folder visualization located in the simulations folder. Then use th efollowing R code to jointly visualize the three SFS.

```{r eval=FALSE}
#Function to normalize after removal of monomorphic sites
norm <- function(x) x/sum(x)


#################Processing the SFS from analysis01 (equilibrium model)  ###############################################

#Read in the SFS from the equilibrium model (no pop. 2)
#Removing the monomorphic classes by subsetting from 2 to 40
sfs_anal01<-scan("analysis01_DAFpop0.obs", skip=2)
sfs_anal01_mono<-sfs_anal01[2:(length(sfs_anal01)-1)]
#normalizing to be able to compare to with SFSs composed of different total number of polymorphic sites
sfs_anal01_mono_norm <- norm(sfs_anal01_mono)


#################Processing the SFS from analysis02 (expansion model)  ###############################################

#Read in the SFS from the expansion model (pop. size increase)
sfs_anal02<-scan("analysis02_DAFpop0.obs", skip=2)
#Removing the monomorphic classes by subsetting from 2 to 40
sfs_anal02_mono<-sfs_anal02[2:(length(sfs_anal02)-1)]
#normalizing to be able to compare to SFS composed of a different total number of polymorphic sites
sfs_anal02_mono_norm <- norm(sfs_anal02_mono)


#################Processing the SFS from analysis03 (crash model)  ###############################################

#Read in the SFS from the crash model (pop. size decrease)
sfs_anal03<-scan("analysis03_DAFpop0.obs", skip=2)
#Removing the monomorphic classes by subsetting from 2 to 40
sfs_anal03_mono<-sfs_anal03[2:(length(sfs_anal03)-1)]
#normalizing to be able to compare to SFS composed of a different total number of polymorphic sites
sfs_anal03_mono_norm <- norm(sfs_anal03_mono)


#Creating and printing the barplot in a pdf format
pdf("equilibrium_expansion_crash.pdf", paper="a4r")
barplot(rbind(sfs_anal02_mono_norm, sfs_anal01_mono_norm, sfs_anal03_mono_norm), beside=T, col=rainbow(3))
legend("topright", legend=c("expansion", "equilibrium", "crash"), fill=rainbow(3))
dev.off()

```

Based on what you learned during the lecture, try to visualize in your mind (or sketch on a piece of
paper) the topologies of the coalescent trees that generated these simulated SFS.

##Exercice 2

Repeat exercise 1 but this time investigating the effect of:

1. Different intensities of population size expansions
2. Different intensities of population size crashes
3. Different population sizes

Note: Don´t overwrite the previous folders (i.e. analysis01, etc...). Just copy/create new folders and
update the *par files and the compare.r script to visualize the results.
**The aim of these exercises is to show you the relation between demographic history and the
distribution of allele frequency**

##Evercise 3

In exercise 1 and 2 we have simulated a SFS given a demographic model. In this exercise we will
**estimate a demographic model given an observed SFS**. (note: Here, we will use a simulated SFS
from exercise 1 but this could as well be the SFS calculated on real data). Here's a
short tutorial to get you started:

1. Go in the folder called *estimations* (he's located next to the simulations folder).
2. Enter the folder called *re_estimating_parameters_of_analysis02* .(*analysis02* was the population
size expansion analysis). The folder should contain the following files.

* *analysis02.est* the estimation file
* *analysis02.tpl* the template file
* *analysis02_DAFpop0.obs* the observed SFS (in our case it is the one we have simulated in analysis02)


let's have a look at *analysis02.tpl*.

```{}
>cat analysis02.tpl

//Number of population samples (demes)
1
//Population effective sizes (number of genes)
N_PRESENT
//Sample sizes
40
//Growth rates
0
//Number of migration matrices : 0 implies no migration between demes
0
//Historical events: time, source, sink, migrants, new size, growth rate, migr. matrix
1 historical event
TIME 0 0 0 REL_SIZE 0 0
//Number of independent loci (contigs), same (0) or differents (1) contig structures
1 0
//Per chromosome: Number of linkage blocks
1
//per Block: data type, num
```

This template file has a format that is similar to the format of the parameter file (*.par) from exercise 1
and 2. It is used to define the “architecture” of the demographic model (i.e. the set of parameters that compose the demographic model). In this case, the model is the same one as in *analysis02* in exercise 1 (
population size expansion). This model has three parameters that we will try to estimate:

1. N_PRESENT: The size of the population before the size change (looking backward in time)
2. TIME: The age of the expansion
3. N_ANCESTRAL: The size of the population after the size change

The first difference between the parameter file (.par) from exercise 1 and this template file (.tpl)
is that instead of specifying numerical values for these three parameters we are replacing
them by variable names (here *N_PRESENT*, *TIME*, *N_ANCESTRAL*). The second difference is
that unlike exercise 1 we do not aim to simulate a genetic dataset, and therefore information relative to the type
of genetic marker, the number of loci, etc... is irrelevant here. Instead we need to tell *fsc2* to
estimate the parameters from the SFS which is done on the last line of the template file. See p.39 of
the manual or ask the teacher for further information about the estimation procedure. We will discuss the importance of the OUTEXP option on the last line of the file later in this tutorial.

The other important file is the estimation file, here *analysis02.est*

```{}
>cat analysis02.est

//Priors and rules file
//*********************
[PARAMETERS]
//#IsInt? #name #dist. #min #max
//All N are in haploid individuals
1 N_PRESENT unif 100 1000000
1 N_ANCESTRAL unif 100 1000000
1 TIME unif 100 100000
[RULES]
[COMPLEX PARAMETERS]
0 REL_SIZE = N_ANCESTRAL/N_PRESENT
```

This file is composed of three sub-sections (*PARAMETERS*, *RULES*, *COMPLEX PARAMETERS*).
In the parameter section we recognize two of the variables that we used in the
corresponding template file (analysis02.tpl) *N_PRESENT* and *TIME*. Fastsimcoal2 implements an iterative algorithm to find the maximum likelihood estimates of the parameters of the demographic model. This algorithm requires that the parameters are initialized to some random values before proceeding with likelihood optimisation. The values 100 and 1000000 represent the boundaries of the range within which initial parameter values will be drawn.

The first column (i.e. IsInt?) indicates if the parameter value is expected to be an integer
(e.g. generation times, individuals) or a floating value (e.g. migration rates, growth rates). Note that
in this example the parameter *N_ANCESTRAL* cannot be used directly in the template file (*REL_SIZE* is
used instead to rescale the population size at the expansion time). The sub-section *COMPLEX PARAMETER* can be used to transform the parameters defined in the *PARAMETERS* section.
Here, we use the *COMPLEX PARAMETER* section to transform the absolute population size of the ancestral population to *REL_SIZE*, the relative ancestral population size as it is required by format of the template file.

Finally, we need to have the observed data (here analysis02_DAFpop0.obs) in the same folder as
the *fsc26* executable. The name of this file has to be formated in the following way:

>*NAMEOFPROJECT_DAFpop0.obs*, where *NAMEOFPROJECT* = analysis02 in this example

We can now estimate the three parameters by executing the following command

```{}
  >fsc26 -t analysis02.tpl -e analysis02.est -d -n 100000 -N 100000 -M 0.001 -l 1 -L 40 -x
```

or by executing the shell script containing this commands
```{}
> bash launch_fsc2_estimations.sh
```

You can check p. 44 of the manual for a description of the command line options. The concepts
behind these options should have been discussed during the lecture. If not ask the teacher.

When the estimations are completed you should see a new folder called *analysis02* that contains the
results of the analysis. The file *analysis02.bestlhoods* contains the estimates for our three
parameters as well as the likelihood of the model given these estimates.
In this example, where the observed SFS was actually derived from simulations (see exercise 1)
we know the true parameter values and therefore have the possibility to compare true and
estimated values. This comparison between a known and estimated parameter value is central to
analyses that aim at testing the performance of an estimation procedure in terms of precision and
bias. However, a single comparison might not be enough to precisely evaluate the performance of
fsc2, thus several similar comparisons are needed (several simulated SFS with the same parameter
values). How could you quickly evaluate how well fsc2 estimated the parameters of the analysis02
model?

##Exercise 4

Repeat exercise 3 but this time you will assume that the model in your template file is an
equilibrium model with a single parameter: the population size N. You can reformat all file names
using *analysis02b* instead of *analysis02*.

* Once the estimation is completed, compare the estimated value of N under this (wrong) model
(equilibrium) with the one you obtained with the correct model (expansion).

* In the files *analysis02.bestlhoods* and *analysis02b.bestlhoods*, look for the *MaxEstLhood* values
and compare them using an **Akaike Information Criteria** (discussed in class). Which model is more
supported by the data?
This exercise is meant to show you how to search for the model that is best supported by the
observed SFS. Remember that you won't know which is the true model when analyzing real NGS
data.

